import Dispatcher from '../dispatcher';
import ActionTypes from '../constants';
 
class Actions {
  changeColor(item) {
    Dispatcher.dispatch({
      actionType: ActionTypes.CHANGE_COLOR,
      payload: item 
    });
  }
}
export default new Actions();