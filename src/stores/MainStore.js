import { EventEmitter } from 'events';
import Dispatcher from '../dispatcher';
import ActionTypes from '../constants';

//const CHANGE = 'CHANGE';

class MainStore extends EventEmitter {
	constructor() {
        super();

        Dispatcher.register(this._registerToActions.bind(this));
    }

    // Switches over the action's type when an action is dispatched.
    _registerToActions(action) {
        switch(action.actionType) {
            case ActionTypes.CART_CHANGE:
                this._cartChange(action.payload);
            break;
            default:
            break;
        }
    }

    _cartChange = (item) =>{
        //Cart.Change(item.variant_id,item.timestamp,item.value);
        //console.log(item);
        //_mode = page
        this.emit('SOMETHING');
    };

	// Hooks a React component's callback to the CHANGED event.
    addChangeListener = (callback,event_name) => {
        this.on(event_name, callback);
    };
 
    // Removes the listener from the CHANGED event.
    removeChangeListener = (callback,event_name) => {
        this.removeListener(event_name, callback);
    };
}

export default new MainStore();