import React, {Component} from 'react';
//import Actions from '../actions/Actions';
import MainStore from '../stores/MainStore.js';

class CartPageTotal extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  };

  UNSAFE_componentWillMount = () =>{
    MainStore.addChangeListener(this._onModeChange,'SOMETHING');
  };

  UNSAFE_componentWillUnmount = () =>{
    MainStore.removeChangeListener(this._onModeChange,'SOMETHING');
  };

  _onModeChange = () =>{
    this.setState({random:Math.random()});
  };

  render() {
    let total = 0;
    for(let key in MainStore.cart_items ){
      let item = MainStore.cart_items[key];
      if( item.type==="main" ){
        if( item.quantity<=0 ) continue;
        total+=item.price*item.quantity;
      }
    }

    return (
      <div>
        {window.Shopify.formatMoney(total)}
      </div>
    )
  };
}

export default CartPageTotal;